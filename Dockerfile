FROM python:3.8-alpine

RUN pip3 install flask

COPY . .

CMD ["python3", "app1.py"]